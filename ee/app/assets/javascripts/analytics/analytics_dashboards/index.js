import Vue from 'vue';
import VueApollo from 'vue-apollo';
import createDefaultClient from '~/lib/graphql';
import { convertObjectPropsToCamelCase, convertArrayToCamelCase } from '~/lib/utils/common_utils';
import DashboardsApp from './dashboards_app.vue';
import createRouter from './router';

const buildAnalyticsDashboardPointer = (analyticsDashboardPointerJSON = '') => {
  return analyticsDashboardPointerJSON.length
    ? convertObjectPropsToCamelCase(JSON.parse(analyticsDashboardPointerJSON))
    : null;
};

export default () => {
  const el = document.getElementById('js-analytics-dashboards-list-app');

  if (!el) {
    return false;
  }

  const {
    dashboardProject: analyticsDashboardPointerJSON = '',
    trackingKey,
    projectId,
    projectFullPath,
    collectorHost,
    chartEmptyStateIllustrationPath,
    dashboardEmptyStateIllustrationPath,
    routerBase,
    features,
  } = el.dataset;

  const analyticsDashboardPointer = buildAnalyticsDashboardPointer(analyticsDashboardPointerJSON);

  Vue.use(VueApollo);

  const apolloProvider = new VueApollo({
    defaultClient: createDefaultClient(
      {},
      {
        cacheConfig: {
          typePolicies: {
            Project: {
              fields: {
                productAnalyticsDashboards: {
                  keyArgs: ['projectPath', 'slug'],
                },
              },
            },
            ProductAnalyticsDashboard: {
              keyFields: ['slug'],
            },
          },
        },
      },
    ),
  });

  return new Vue({
    el,
    apolloProvider,
    router: createRouter(routerBase),
    provide: {
      customDashboardsProject: analyticsDashboardPointer,
      trackingKey,
      projectFullPath,
      projectId,
      collectorHost,
      chartEmptyStateIllustrationPath,
      dashboardEmptyStateIllustrationPath,
      features: convertArrayToCamelCase(JSON.parse(features)),
    },
    render(h) {
      return h(DashboardsApp);
    },
  });
};
